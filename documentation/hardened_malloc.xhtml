<!DOCTYPE html>

<!-- Inferencium - Website - Documentation - hardened_malloc -->
<!-- Version: 5.0.3-alpha.2 -->

<!-- Copyright 2023 Jake Winters -->
<!-- SPDX-License-Identifier: BSD-3-Clause -->


<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<meta charset="utf-8"/>
		<meta name="viewport" content="width=device-width, initial-scale=1"/>
		<link rel="stylesheet" href="../main.css"/>
		<link rel="icon shortcut" href="../asset/img/logo/inferencium-notext.png"/>
		<title>Inferencium - Documentation - hardened_malloc</title>
	</head>
	<body>
		<nav class="navbar">
			<div class="logo"><a href="../index.xhtml"><img src="../asset/img/logo/inferencium-notext.png" alt="Inferencium logo"/></a></div>
			<div class="title"><a href="../index.xhtml">Inferencium</a></div>
			<div><a href="../about.xhtml">About</a></div>
			<div><a href="../news.xhtml">News</a></div>
			<div><a href="../documentation.xhtml">Documentation</a></div>
			<div><a href="../source.xhtml">Source</a></div>
			<div><a href="../changelog.xhtml">Changelog</a></div>
			<div><a href="../blog.xhtml">Blog</a></div>
			<div><a href="../contact.xhtml">Contact</a></div>
			<div><a href="../directory.xhtml">Directory</a></div>
			<div><a href="../key.xhtml">Key</a></div>
			<div class="sitemap"><a href="../sitemap.xhtml">Sitemap</a></div>
		</nav>
		<h1 id="hardened_malloc"><a href="#hardened_malloc">Documentation - hardened_malloc</a></h1>
			<section id="introduction">
				<p>This documentation contains instructions to use
				<a href="https://github.com/GrapheneOS/hardened_malloc">hardened_malloc</a>
				memory allocator as the system's default memory allocator via dynamic linking as a shared library. These
				instructions apply to both musl and glibc C libraries on Linux-based systems.</p>
				<p>hardened_malloc can also be used per-application and/or per-user, in which case root permissions are
				not required; this documentation focuses on system-wide usage of hardened_malloc, assumes root
				privileges, and assumes the compiled library will readable and executable by all users of the
				system.</p>
				<p>This documentation uses
				<a href="https://refspecs.linuxfoundation.org/FHS_3.0/fhs/index.html">Linux Filesystem Hierarchy Standard</a>
				paths, with the modern <code>/usr/</code> merge approach of most Linux distributions. For non-standard
				configurations, adjust the paths accordingly.</p>
				<p>For the complete hardened_malloc documentation, visit its
				<a href="https://github.com/GrapheneOS/hardened_malloc/?tab=readme-ov-file#hardened-malloc">official documentation</a>.</p>
				<p>This documentation is also available in portable
				<a href="https://src.inferencium.net/Inferencium/doc/src/branch/stable/security/hardened_malloc.adoc">AsciiDoc format</a>
				in my documentation source code repository.</p>
			</section>
			<nav id="toc">
				<h2><a href="#toc">Table of Contents</a></h2>
					<ul>
						<li><a href="#memory_pages">Increase Permitted Amount of Memory Pages</a></li>
						<li><a href="#clone_source_code">Clone hardened_malloc Source Code</a></li>
						<li><a href="#enter_local_repository">Enter hardened_malloc Local Git Repository</a></li>
						<li><a href="#compile">Compile hardened_malloc</a></li>
						<li><a href="#copy_library">Copy Compiled hardened_malloc Library</a></li>
						<li><a href="#preload_on_boot">Set System to Preload hardened_malloc on Boot</a></li>
					</ul>
			</nav>
			<section id="memory_pages">
				<h2><a href="#memory_pages">Increase Permitted Amount of Memory Pages</a></h2>
					<p>Add the following to <code>/etc/sysctl.conf</code> or a configuration file within
					<code>/etc/sysctl.d/</code>to accommodate hardened_malloc's large amount of guard pages:</p>
					<pre>vm.max_map_count = 1048576</pre>
			</section>
			<section id="clone_source_code">
				<h2><a href="#clone_source_code">Clone hardened_malloc Source Code</a></h2>
					<pre>$ git clone https://github.com/GrapheneOS/hardened_malloc.git</pre>
			</section>
			<section id="enter_local_repository">
				<h2><a href="#enter_local_repository">Enter hardened_malloc Local Git Repository</a></h2>
					<pre>$ cd hardened_malloc/</pre>
			</section>
			<section id="compile">
				<h2><a href="#compile">Compile hardened_malloc</a></h2>
					<pre>$ make <var>&lt;arguments&gt;</var></pre>
					<p><code>CONFIG_N_ARENA=<var>n</var></code> can be adjusted to increase parallel performance at the
					expense of memory usage, or decrease memory usage at the expense of parallel performance, where
					<code><var>n</var></code> is a non-negative integer. Higher values prefer parallel performance,
					whereas lower values prefer lower memory usage. Note that having too many arenas may cause memory
					fragmentation and decrease system performance. The number of arenas has no impact on the security
					properties of hardened_malloc.</p>
						<div style="overflow-x:auto;">
							<table align="center">
								<thead>
									<tr>
										<th id="arena-min">Minimum</th>
										<th id="arena-max">Maximum</th>
										<th id="arena-def">Default</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td headers="arena-min">1</td>
										<td headers="arena-max">256</td>
										<td headers="arena-def">4</td>
									</tr>
								</tbody>
							</table>
						</div>
					<p>For extra security, <code>CONFIG_SEAL_METADATA=true</code> can be used in order to control
					whether
					<a href="https://www.kernel.org/doc/html/v6.9/core-api/protection-keys.html">Memory Protection Keys</a>
					are used to disable access to all writable allocator state outside of the memory allocator code.
					It's currently disabled by default due to a significant performance cost for this use case on
					current-generation hardware. Whether or not this feature is enabled, the metadata is all contained
					within an isolated memory region with high-entropy random guard regions around it.</p>
					<p>For low-memory systems, <code>VARIANT=light</code> can be used to compile the light variant of
					hardened_malloc, which sacrifices some security for less memory usage. This option still produces a
					more hardened memory allocator than both the default musl and glibc allocators, despite the security
					sacrifices over the full variant.</p>
					<p>For all compile-time options, see the
					<a href="https://github.com/GrapheneOS/hardened_malloc#configuration">configuration section</a>
					of hardened_malloc's official documentation.</p>
			</section>
			<section id="copy_library">
				<h2><a href="#copy_library">Copy Compiled hardened_malloc Library</a></h2>
					<pre># cp out/libhardened_malloc.so /usr/local/lib/libhardened_malloc.so</pre>
			</section>
			<section id="preload_on_boot">
				<h2><a href="#preload_on_boot">Set System to Preload hardened_malloc on Boot</a></h2>
					<p>In order to preload the hardened_malloc shared library on boot, perform the following
					actions:</p>
					<p><b>musl-based systems:</b> Add the following to <code>/etc/environment</code> or a configuration
					file within <code>/etc/environment.d/</code>:</p>
					<pre>LD_PRELOAD=/usr/local/lib/libhardened_malloc.so</pre>
					<p><b>glibc-based systems:</b> Add the following to <code>/etc/ld.so.preload</code>:</p>
					<pre>/usr/local/lib/libhardened_malloc.so</pre>
			</section>
		<div class="sitemap-small"><a href="../sitemap.xhtml">Sitemap</a></div>
	</body>
</html>
