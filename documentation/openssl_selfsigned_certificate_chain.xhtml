<!DOCTYPE html>

<!-- Inferencium - Website - Documentation - OpenSSL Self-signed Certificate Chain -->
<!-- Version: 5.0.2-alpha.3 -->

<!-- Copyright 2023 Jake Winters -->
<!-- SPDX-License-Identifier: BSD-3-Clause -->


<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<meta charset="utf-8"/>
		<meta name="viewport" content="width=device-width, initial-scale=1"/>
		<link rel="stylesheet" href="../main.css"/>
		<link rel="icon shortcut" href="../asset/img/logo/inferencium-notext.png"/>
		<title>Inferencium - Documentation - OpenSSL Self-signed Certificate Chain</title>
	</head>
	<body>
		<nav class="navbar">
			<div class="logo"><a href="../index.xhtml"><img src="../asset/img/logo/inferencium-notext.png" alt="Inferencium logo"/></a></div>
			<div class="title"><a href="../index.xhtml">Inferencium</a></div>
			<div><a href="../about.xhtml">About</a></div>
			<div><a href="../news.xhtml">News</a></div>
			<div><a href="../documentation.xhtml">Documentation</a></div>
			<div><a href="../source.xhtml">Source</a></div>
			<div><a href="../changelog.xhtml">Changelog</a></div>
			<div><a href="../blog.xhtml">Blog</a></div>
			<div><a href="../contact.xhtml">Contact</a></div>
			<div><a href="../directory.xhtml">Directory</a></div>
			<div><a href="../key.xhtml">Key</a></div>
			<div class="sitemap"><a href="../sitemap.xhtml">Sitemap</a></div>
		</nav>
		<h1 id="openssl_selfsigned_certificate_chain"><a href="#openssl_selfsigned_certificate_chain">Documentation - OpenSSL Self-signed Certificate Chain</a></h1>
			<section id="introduction">
				<p>This documentation contains the complete set of commands to create a new OpenSSL self-signed
				certificate chain with V3 subjectAltName (SAN) extensions enabled. SANs can be included in a certificate
				by adding each domain as a comma-delimited string.</p>
				<p>Each key can be encrypted or unencrypted, with multiple encryption options; AES (<code>aes128</code>
				or <code>aes256</code>) is recommended.</p>
				<p>Optional verification can also be performed between multiple levels of certificates to ensure the chain
				of trust is valid.</p>
				<p>This documentation is also available in portable
				<a href="https://src.inferencium.net/Inferencium/doc/src/branch/stable/security/openssl_selfsigned_certificate_chain.adoc">AsciiDoc format</a>
				in my documentation source code repository.</p>
			</section>
			<nav id="toc">
				<h2><a href="#toc">Table of Contents</a></h2>
					<ul>
						<li><a href="#create_certificate_authority_key">Create Certificate Authority Key</a></li>
						<li><a href="#verify_certificate_authority_key">Verify Certificate Authority Key</a></li>
						<li><a href="#create_certificate_authority_certificate">Create Certificate Authority Certificate</a></li>
						<li><a href="#convert_certificate_to_pem_format">Convert Certificate to PEM Format</a></li>
						<li><a href="#verify_certificate_authority_certificate">Verify Certificate Authority Certificate</a></li>
						<li><a href="#create_intermediate_certificate_authority_key">Create Intermediate Certificate Authority Key</a></li>
						<li><a href="#verify_intermediate_certificate_authority_key">Verify Intermediate Certificate Authority Key</a></li>
						<li><a href="#create_intermediate_certificate_authority_signing_request">Create Intermediate Certificate Signing Request</a></li>
						<li><a href="#create_intermediate_certificate_authority_certificate">Create Intermediate Certificate Authority Certificate</a></li>
						<li><a href="#verify_intermediate_certificate_authority_certificate">Verify Intermediate Certificate Authority Certificate</a></li>
						<li><a href="#verify_chain_of_trust-ca_to_intermediate">Verify Chain of Trust (CA to Intermediate)</a></li>
						<li><a href="#create_server_key">Create Server Key</a></li>
						<li><a href="#verify_server_key">Verify Server Key</a></li>
						<li><a href="#create_server_certificate_signing_request">Create Server Cerificate Signing Request</a></li>
						<li><a href="#create_server_certificate">Create Server Certificate</a></li>
						<li><a href="#verify_server_certificate">Verify Server Certificate</a></li>
						<li><a href="#verify_chain_of_trust-intermediate_to_server">Verify Chain of Trust (Intermediate to Server)</a></li>
					</ul>
			</nav>
			<section id="create_certificate_authority_key">
				<h2><a href="#create_certificate_authority_key">Create Certificate Authority Key</a></h2>
					<pre>openssl genrsa <var>&lt;encryption type&gt;</var> -out <var>&lt;CA key name&gt;</var>.pem <var>&lt;key size&gt;</var></pre>
			</section>
			<section id="verify_certificate_authority_key">
				<h2><a href="#verify_certificate_authority_key">Verify Certificate Authority Key</a></h2>
					<pre>openssl rsa -noout -text -in <var>&lt;CA key name&gt;</var>.pem</pre>
			</section>
			<section id="create_certificate_authority_certificate">
				<h2><a href="#create_certificate_authority_certificate">Create Certificate Authority Certificate</a></h2>
					<pre>openssl req -new -x509 -days <var>&lt;days of validity&gt;</var> -extensions v3_ca -key <var>&lt;CA key name&gt;</var>.pem -out <var>&lt;CA certificate name&gt;</var>.pem</pre>
			</section>
			<section id="convert_certificate_to_pem_format">
				<h2><a href="#convert_certificate_to_pem_format">Convert Certificate to PEM Format</a></h2>
					<pre>openssl x509 -in <var>&lt;CA certificate name&gt;</var>.pem -out <var>&lt;CA certificate name&gt;</var>.pem -outform PEM</pre>
			</section>
			<section id="verify_certificate_authority_certificate">
				<h2><a href="#verify_certificate_authority_certificate">Verify Certificate Authority Certificate</a></h2>
					<pre>openssl x509 -noout -text -in <var>&lt;CA certificate name&gt;</var>.pem</pre>
			</section>
			<section id="create_intermediate_certificate_authority_key">
				<h2><a href="#create_intermediate_certificate_authority_key">Create Intermediate Certificate Authority Key</a></h2>
					<pre>openssl genrsa <var>&lt;encryption type&gt;</var> -out <var>&lt;intermediate CA key name&gt;</var>.pem <var>&lt;key size&gt;</var></pre>
			</section>
			<section id="verify_intermediate_certificate_authority_key">
				<h2><a href="#verify_intermediate_certificate_authority_key">Verify Intermediate Certificate Authority Key</a></h2>
					<pre>openssl rsa -noout -text -in <var>&lt;intermediate CA key name&gt;</var>.pem</pre>
			</section>
			<section id="create_intermediate_certificate_authority_signing_request">
				<h2><a href="#create_intermediate_certificate_authority_signing_request">Create Intermediate Certificate Authority Signing Request</a></h2>
					<pre>openssl req -new -sha256 -key <var>&lt;intermediate CA key name&gt;</var>.pem -out <var>&lt;intermediate CA certificate signing request name&gt;</var>.pem</pre>
			</section>
			<section id="create_intermediate_certificate_authority_certificate">
				<h2><a href="#create_intermediate_certificate_authority_certificate">Create Intermediate Certificate Authority Certificate</a></h2>
					<pre>openssl ca -config <var>&lt;intermediate CA configuration file&gt;</var> -extensions v3_intermediate_ca -days <var>&lt;days of validity&gt;</var> -notext -md sha256 -in <var>&lt;intermediate CA signing request name&gt;</var>.pem -out <var>&lt;intermediate CA certificate name&gt;</var>.pem</pre>
			</section>
			<section id="verify_intermediate_certificate_authority_certificate">
				<h2><a href="#verify_intermediate_certificate_authority_certificate">Verify Intermediate Certificate Authority Certificate</a></h2>
					<pre>openssl x509 -noout -text -in <var>&lt;intermediate CA certificate name&gt;</var>.pem</pre>
			</section>
			<section id="verify_chain_of_trust-ca_to_intermediate">
				<h2><a href="#verify_chain_of_trust-ca_to_intermediate">Verify Chain of Trust (CA to Intermediate)</a></h2>
					<pre>openssl verify -CAfile <var>&lt;CA certificate name&gt;</var>.pem <var>&lt;intermediate CA certificate name&gt;</var>.pem</pre>
			</section>
			<section id="create_server_key">
				<h2><a href="#create_server_key">Create Server Key</a></h2>
					<pre>openssl genrsa <var>&lt;encryption type&gt;</var> -out <var>&lt;server key name&gt;</var>.pem <var>&lt;key size&gt;</var></pre>
			</section>
			<section id="verify_server_key">
				<h2><a href="#verify_server_key">Verify Server Key</a></h2>
					<pre>openssl rsa -noout -text -in <var>&lt;server key name&gt;</var>.pem</pre>
			</section>
			<section id="create_server_certificate_signing_request">
				<h2><a href="#create_server_certificate_signing_request">Create Server Certificate Signing Request</a></h2>
					<pre>openssl req -new -sha256 -subj "/C=<var>&lt;country&gt;</var>/ST=<var>&lt;state/province&gt;</var>/L=<var>&lt;locality&gt;</var>/O=<var>&lt;organization&gt;</var>/CN=<var>&lt;common name&gt;</var>" -addext "subjectAltName = DNS.1:<var>&lt;alternative DNS entry&gt;</var>" -key <var>&lt;server key name&gt;</var>.pem -out <var>&lt;server certificate signing request name&gt;</var>.pem</pre>
			</section>
			<section id="create_server_certificate">
				<h2><a href="#create_server_certificate">Create Server Certificate</a></h2>
					<pre>openssl x509 -sha256 -req  -days <var>&lt;days of validity&gt;</var> -in <var>&lt;server certificate signing request name&gt;</var>.pem -CA <var>&lt;intermediate CA certificate name&gt;</var>.pem -CAkey <var>&lt;intermediate CA key name&gt;</var>.pem -extensions SAN -extfile &lt;(cat /etc/ssl/openssl.cnf &lt;(printf "\n[SAN]\nsubjectAltName=DNS.1:")) -out <var>&lt;server certificate name&gt;</var>.pem</pre>
			</section>
			<section id="verify_server_certificate">
				<h2><a href="#verify_server_certificate">Verify Server Certificate</a></h2>
					<pre>openssl x509 -noout -text -in <var>&lt;server certificate name&gt;</var>.pem</pre>
			</section>
			<section id="verify_chain_of_trust-intermediate_to_server">
				<h2><a href="#verify_chain_of_trust-intermediate_to_server">Verify Chain of Trust (Intermediate to Server)</a></h2>
					<pre>openssl verify -CAfile <var>&lt;intermediate CA certificate name&gt;</var>.pem <var>&lt;server certificate&gt;</var>.pem</pre>
			</section>
		<div class="sitemap-small"><a href="../sitemap.xhtml">Sitemap</a></div>
	</body>
</html>
