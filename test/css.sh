#!/bin/zsh


# Inferencium - Website - Test - CSS
# Copyright 2024 Jake Winters

# Version: 0.1.0
# SPDX-License-Identifier: BSD-3-Clause


# Location
root=".." # Website root directory

# Formatting
bold="\033[1m" # Bold
ok="\033[0;32;1mOK\033[0m" # Green + Bold
fail="\033[0;31;1mFAIL\033[0m" # Red + Bold
nof="\033[0m" # None


# Check for existence of CSS
echo "${bold}Checking for existence of CSS...${nof}"

echo ""
echo "${bold}Root${nof}"

if [ -f $root/main.css ]; then
	echo "main.css						${ok}"
else
	echo "main.css						${fail}"
fi
