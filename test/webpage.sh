#!/bin/zsh


# Inferencium - Website - Test - Webpages
# Copyright 2024 Jake Winters

# Version: 0.2.0
# SPDX-License-Identifier: BSD-3-Clause


# Location
root=".." # Website root directory
blog="../blog" # Website blog directory
clog="../changelog" # Changelog directory
doc="../documentation" # Website documentation directory

# Formatting
bold="\033[1m" # Bold
ok="\033[0;32;1mOK\033[0m" # Green + Bold
fail="\033[0;31;1mFAIL\033[0m" # Red + Bold
nof="\033[0m" # None


# Check for existence of webpages
echo "${bold}Checking for existence of webpages...${nof}"

echo ""
echo "${bold}Root${nof}"

if [ -f $root/about.xhtml ]; then
	echo "about.xhtml						${ok}"
else
	echo "about.xhtml						${fail}"
fi

if [ -f $root/ads.txt ]; then
	echo "ads.txt							${ok}"
else
	echo "ads.txt							${fail}"
fi

if [ -f $root/app-ads.txt ]; then
	echo "app-ads.txt						${ok}"
else
	echo "app-ads.txt						${fail}"
fi

if [ -f $root/blog.xhtml ]; then
	echo "blog.xhtml						${ok}"
else
	echo "blog.xhtml						${fail}"
fi

if [ -f $root/changelog.xhtml ]; then
	echo "changelog.xhtml						${ok}"
else
	echo "changelog.xhtml						${fail}"
fi

if [ -f $root/contact.xhtml ]; then
	echo "contact.xhtml						${ok}"
else
	echo "contact.xhtml						${fail}"
fi

if [ -f $root/directory.xhtml ]; then
	echo "directory.xhtml						${ok}"
else
	echo "directory.xhtml						${fail}"
fi

if [ -f $root/documentation.xhtml ]; then
	echo "documentation.xhtml					${ok}"
else
	echo "documentation.xhtml					${fail}"
fi

if [ -f $root/humans.txt ]; then
	echo "humans.txt						${ok}"
else
	echo "humans.txt						${fail}"
fi

if [ -f $root/index.xhtml ]; then
	echo "index.xhtml						${ok}"
else
	echo "index.xhtml						${fail}"
fi

if [ -f $root/key.xhtml ]; then
	echo "key.xhtml						${ok}"
else
	echo "key.xhtml						${fail}"
fi

if [ -f $root/music.xhtml ]; then
	echo "music.xhtml						${ok}"
else
	echo "music.xhtml						${fail}"
fi

if [ -f $root/news.xhtml ]; then
	echo "news.xhtml						${ok}"
else
	echo "news.xhtml						${fail}"
fi

if [ -f $root/robots.txt ]; then
	echo "robots.txt						${ok}"
else
	echo "robots.txt						${fail}"
fi

if [ -f $root/sitemap.xhtml ]; then
	echo "sitemap.xhtml						${ok}"
else
	echo "sitemap.xhtml						${fail}"
fi

if [ -f $root/sitemap.xml ]; then
	echo "sitemap.xml						${ok}"
else
	echo "sitemap.xml						${fail}"
fi

if [ -f $root/source.xhtml ]; then
	echo "source.xhtml						${ok}"
else
	echo "source.xhtml						${fail}"
fi

echo ""
echo "${bold}Blog${nof}"

if [ -f $blog/foss_is_working_against_itself.xhtml ]; then
	echo "foss_is_working_against_itself.xhtml			${ok}"
else
	echo "foss_is_working_against_itself.xhtml			${fail}"
fi

if [ -f $blog/systemd_insecurity.xhtml ]; then
	echo "systemd_insecurity.xhtml				${ok}"
else
	echo "systemd_insecurity.xhtml				${fail}"
fi

if [ -f $blog/the_chromium_monopoly.xhtml ]; then
	echo "the_chromium_monopoly.xhtml				${ok}"
else
	echo "the_chromium_monopoly.xhtml				${fail}"
fi

if [ -f $blog/untrusted_the_issue_with_decentralisation.xhtml ]; then
	echo "untrusted_the_issue_with_decentralisation.xhtml		${ok}"
else
	echo "untrusted_the_issue_with_decentralisation.xhtml		${fail}"
fi

echo ""
echo "${bold}Changelog${nof}"

if [ -f $clog/firmware-aa000-0.xhtml ]; then
	echo "firmware-aa000-0.xhtml					${ok}"
else
	echo "firmware-aa000-0.xhtml					${fail}"
fi

if [ -f $clog/firmware-xa000-0.xhtml ]; then
	echo "firmware-xa000-0.xhtml					${ok}"
else
	echo "firmware-xa000-0.xhtml					${fail}"
fi

if [ -f $clog/firmware-xb000-0.xhtml ]; then
	echo "firmware-xb000-0.xhtml					${ok}"
else
	echo "firmware-xb000-0.xhtml					${fail}"
fi

echo ""
echo "${bold}Documentation${nof}"

if [ -f $doc/hardened_malloc.xhtml ]; then
	echo "hardened_malloc.xhtml					${ok}"
else
	echo "hardened_malloc.xhtml					${fail}"
fi

if [ -f $doc/openssl_selfsigned_certificate_chain.xhtml ]; then
	echo "openssl_selfsigned_certificate_chain.xhtml		${ok}"
else
	echo "openssl_selfsigned_certificate_chain.xhtml		${fail}"
fi
