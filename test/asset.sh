#!/bin/zsh


# Inferencium - Website - Test - Assets
# Copyright 2024 Jake Winters

# Version: 0.2.0
# SPDX-License-Identifier: BSD-3-Clause


# Location
asset="../asset" # Website asset directory
avatar="../asset/img/avatar" # Website avatar asset directory
img="../asset/img" # Website image asset directory
logo="../asset/img/logo" # Website logo asset directory

# Formatting
bold="\033[1m" # Bold
ok="\033[0;32;1mOK\033[0m" # Green + Bold
fail="\033[0;31;1mFAIL\033[0m" # Red + Bold
nof="\033[0m" # None


# Check for existence of assets
echo "${bold}Checking for existence of assets...${nof}"

echo ""
echo "${bold}Image${nof}"

if [ -f $img/google-pixel_8_pro.png ]; then
	echo "google-pixel_8_pro.png					${ok}"
else
	echo "google-pixel_8_pro.png					${fail}"
fi

echo ""
echo "${bold}Avatar${nof}"

if [ -f $avatar/inference.png ]; then
	echo "inference.png						${ok}"
else
	echo "inference.png						${fail}"
fi

echo ""
echo "${bold}Logo${nof}"

if [ -f $logo/chromium.png ]; then
	echo "chromium.png						${ok}"
else
	echo "chromium.png						${fail}"
fi

if [ -f $logo/conversations.png ]; then
	echo "conversations.png					${ok}"
else
	echo "conversations.png					${fail}"
fi

if [ -f $logo/gallery.png ]; then
	echo "gallery.png						${ok}"
else
	echo "gallery.png						${fail}"
fi

if [ -f $logo/gentoo_linux.png ]; then
	echo "gentoo_linux.png					${ok}"
else
	echo "gentoo_linux.png					${fail}"
fi

if [ -f $logo/git.png ]; then
	echo "git.png							${ok}"
else
	echo "git.png							${fail}"
fi

if [ -f $logo/gitea.png ]; then
	echo "gitea.png						${ok}"
else
	echo "gitea.png						${fail}"
fi

if [ -f $logo/grapheneos.png ]; then
	echo "grapheneos.png						${ok}"
else
	echo "grapheneos.png						${fail}"
fi

if [ -f $logo/inferencium-notext.png ]; then
	echo "inferencium-notext.png					${ok}"
else
	echo "inferencium-notext.png					${fail}"
fi

if [ -f $logo/molly.png ]; then
	echo "molly.png						${ok}"
else
	echo "molly.png						${fail}"
fi

if [ -f $logo/musl.png ]; then
	echo "musl.png						${ok}"
else
	echo "musl.png						${fail}"
fi

if [ -f $logo/opencalc.png ]; then
	echo "opencalc.png						${ok}"
else
	echo "opencalc.png						${fail}"
fi

if [ -f $logo/signal.png ]; then
	echo "signal.png						${ok}"
else
	echo "signal.png						${fail}"
fi

if [ -f $logo/vanadium.png ]; then
	echo "vanadium.png						${ok}"
else
	echo "vanadium.png						${fail}"
fi

if [ -f $logo/xmpp.png ]; then
	echo "xmpp.png						${ok}"
else
	echo "xmpp.png						${fail}"
fi
